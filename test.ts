import { Injector, Provider } from '@typemon/di';
import { UpdateExpression, listAppend, equal, Expression, size } from '@typemon/dynamodb-expression';
//
import {
    /* DynamoDB service class */
    DynamoDBService,

    /* Injection tokens */
    TABLE_NAME_TOKEN, OPTIONS_TOKEN,

    /* Types, Interfaces */
    Options, QueryOptions, QueryResult,

    /* Empty string namespace */
    EmptyString
} from './src';
//
//
//
interface Monster {
    readonly partition: 'monster';
    readonly name: string;
    readonly description: string;
    readonly packages: ReadonlyArray<string>;
}

const injector: Injector = Injector.create([
    Provider.useValue({
        identifier: TABLE_NAME_TOKEN,
        value: 'monster-space-network'
    }),
    Provider.useValue({
        identifier: OPTIONS_TOKEN,
        value: {
            region: 'ap-northeast-2'
        }
    }),
    Provider.use(DynamoDBService)
]);

const dynamodb: DynamoDBService = injector.get(DynamoDBService);
const tests: ReadonlyArray<() => Promise<void>> = [
    async (): Promise<void> => {
        const typemon: Monster = {
            partition: 'monster', // partition key
            name: 'typemon',      // sort key
            description: 'TypeScript Monster',
            packages: [
                'ioc-container',
                'dynamodb-service',
                'dynamodb-expression'
            ]
        };

        await dynamodb.put(typemon);
    },
    async (): Promise<void> => {
        const typemon: Monster | null = await dynamodb.get({
            partition: 'monster', // partition key
            name: 'typemon',      // sort key
        });

        console.log(typemon);
    },
    async (): Promise<void> => {
        const updateExpressions: ReadonlyArray<UpdateExpression> = [
            listAppend('packages', [
                'serverless',
                'stack'
            ])
        ];
        const typemon: Monster = await dynamodb.update({
            partition: 'monster', // partition key
            name: 'typemon',      // sort key
        }, updateExpressions);

        console.log(typemon);
    },
    async (): Promise<void> => {
        await dynamodb.delete({
            partition: 'monster', // partition key
            name: 'typemon',      // sort key
        });
    },
    async (): Promise<void> => {
        const keyConditionExpression: Expression = equal('partition', 'monster');
        const filterExpression: Expression = equal(size('packages'), 0);
        const monsters: QueryResult<Monster> = await dynamodb.query(keyConditionExpression, filterExpression, { limit: 1 });

        console.log(monsters);
    }
];

tests.reduce((promise: Promise<void>, test: () => Promise<void>): Promise<void> => {
    return promise.then((): Promise<void> => test());
}, tests[0]());
