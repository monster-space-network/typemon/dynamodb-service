//
//
//
export {
    DynamoDBService,
    Options,
    TABLE_NAME_TOKEN, OPTIONS_TOKEN,
    QueryOptions, QueryResult
} from './dynamodb.service';
export { EmptyString } from './empty-string';
