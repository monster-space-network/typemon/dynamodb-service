import { Injectable, InjectionToken, Inject, Optional } from '@typemon/di';
import { UpdateExpression, Expression } from '@typemon/dynamodb-expression';
import { Index, ReadonlyIndex, Type } from '@typemon/types';
import { DocumentClient, Types } from 'aws-sdk/clients/dynamodb';
//
import { EmptyString } from './empty-string';
//
//
//
export const TABLE_NAME_TOKEN: InjectionToken<string> = new InjectionToken('DynamoDBService.TableName');

export interface Options extends DocumentClient.DocumentClientOptions, Types.ClientConfiguration {
    /**
     * @description If you do not want to use empty string handling, set this option to `false`.
     * @default true
     * @see https://gitlab.com/monster-space-network/typemon/dynamodb-service#empty-string-handling
     */
    readonly useEmptyStringHandling?: boolean;
}
export const OPTIONS_TOKEN: InjectionToken<Options> = new InjectionToken('DynamoDBService.Options');

export interface QueryOptions {
    readonly limit?: number;
    readonly exclusiveStartKey?: ReadonlyIndex<string, unknown> | null;
}
export interface QueryResult<Item extends object> {
    readonly items: ReadonlyArray<Item>;
    readonly lastEvaluatedKey: ReadonlyIndex<string, unknown> | null;
}

@Injectable()
export class DynamoDBService {
    protected readonly client: DocumentClient;

    public constructor(
        @Inject(TABLE_NAME_TOKEN) protected readonly tableName: string,
        @Inject(OPTIONS_TOKEN) @Optional() protected readonly options: Options = {}
    ) {
        this.client = new DocumentClient(this.options);
    }

    private serializeEmptyString(target: object): object {
        if (Type.isFalse(this.options.useEmptyStringHandling)) {
            return target;
        }

        return EmptyString.serialize(target);
    }
    private deserializeEmptyString(target: Index<string, unknown>): ReadonlyIndex<string, unknown> {
        if (Type.isFalse(this.options.useEmptyStringHandling)) {
            return target;
        }

        return EmptyString.deserialize(target);
    }

    public async put<Item extends object>(item: Item): Promise<void> {
        await this.client.put({
            TableName: this.tableName,
            Item: this.serializeEmptyString(item)
        }).promise();
    }

    public async get<Item extends object>(primaryKey: ReadonlyIndex<string, unknown>): Promise<Item | null> {
        const output: DocumentClient.GetItemOutput = await this.client.get({
            TableName: this.tableName,
            Key: primaryKey
        }).promise();

        if (Type.isUndefined(output.Item)) {
            return null;
        }

        return this.deserializeEmptyString(output.Item) as Item;
    }

    public async update<PrimaryKey extends ReadonlyIndex<string, unknown>, Item extends object & PrimaryKey>(primaryKey: PrimaryKey, updateExpressions: ReadonlyArray<UpdateExpression>): Promise<Item> {
        const updateExpression: Expression = UpdateExpression.build(updateExpressions);
        const output: DocumentClient.UpdateItemOutput = await this.client.update({
            TableName: this.tableName,
            Key: primaryKey,
            UpdateExpression: updateExpression.expression,
            ExpressionAttributeNames: updateExpression.names,
            ExpressionAttributeValues: this.serializeEmptyString(updateExpression.values),

            ReturnValues: 'ALL_NEW'
        }).promise();

        return this.deserializeEmptyString(output.Attributes as Index<string, unknown>) as Item;
    }

    public async delete(primaryKey: ReadonlyIndex<string, unknown>): Promise<void> {
        await this.client.delete({
            TableName: this.tableName,
            Key: primaryKey
        }).promise();
    }

    public async query<Item extends object>(keyConditionExpression: Expression, filterExpression?: Expression | null, options: QueryOptions = {}): Promise<QueryResult<Item>> {
        const input: DocumentClient.QueryInput = {
            TableName: this.tableName,
            KeyConditionExpression: keyConditionExpression.expression,
            ExpressionAttributeNames: keyConditionExpression.names,
            ExpressionAttributeValues: keyConditionExpression.values,

            Limit: options.limit || undefined,
            ExclusiveStartKey: Type.isNotUndefinedAndNotNull(options.exclusiveStartKey) ? this.serializeEmptyString(options.exclusiveStartKey) : undefined
        };

        if (Type.isNotUndefinedAndNotNull(filterExpression)) {
            input.FilterExpression = filterExpression.expression;
            input.ExpressionAttributeNames = Object.assign(input.ExpressionAttributeNames, this.serializeEmptyString(filterExpression.names));
            input.ExpressionAttributeValues = Object.assign(input.ExpressionAttributeValues, this.serializeEmptyString(filterExpression.values));
        }

        const output: DocumentClient.QueryOutput = await this.client.query(input).promise();
        const items: ReadonlyArray<Item> = Type.isNotUndefined(output.Items) ? output.Items as Array<Item> : [];

        return {
            items: items.map((item: Item): Item => this.deserializeEmptyString(item) as Item),
            lastEvaluatedKey: Type.isUndefined(output.LastEvaluatedKey) ? null : this.deserializeEmptyString(output.LastEvaluatedKey)
        };
    }

    public async exists(keyConditionExpression: Expression, filterExpression?: Expression): Promise<boolean> {
        let result: QueryResult<object> = await this.query(keyConditionExpression, filterExpression);

        if (result.items.length > 0) {
            return true;
        }

        while (Type.isNotNull(result.lastEvaluatedKey)) {
            result = await this.query(keyConditionExpression, filterExpression, {
                exclusiveStartKey: result.lastEvaluatedKey
            });

            if (result.items.length > 0) {
                return true;
            }
        }

        return false;
    }
}
