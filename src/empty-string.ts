import { Type, Index } from '@typemon/types';
//
//
//
export namespace EmptyString {
    export const IDENTIFIER: string = 'EMPTY_STRING_IDENTIFIER';

    export function serialize<Target extends object>(target: Target): Target {
        const copiedTarget: Index<string, unknown> = Type.isArray(target) ? target.slice() : { ...target };
        const keys: ReadonlyArray<string> = Object.keys(copiedTarget);

        for (const key of keys) {
            const value: unknown = copiedTarget[key];

            if (Type.isObject(value)) {
                copiedTarget[key] = serialize(value);
            }
            else if (value === '') {
                copiedTarget[key] = IDENTIFIER;
            }
        }

        return copiedTarget as Target;
    }
    export function deserialize<Target extends object>(target: Target): Target {
        const copiedTarget: Index<string, unknown> = Type.isArray(target) ? target.slice() : { ...target };
        const keys: ReadonlyArray<string> = Object.keys(copiedTarget);

        for (const key of keys) {
            const value: unknown = copiedTarget[key];

            if (Type.isObject(value)) {
                copiedTarget[key] = deserialize(value);
            }
            else if (value === IDENTIFIER) {
                copiedTarget[key] = '';
            }
        }

        return copiedTarget as Target;
    }
}
