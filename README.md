# DynamoDB Service - [![npm-version](https://img.shields.io/npm/v/@typemon/dynamodb-service.svg)](https://www.npmjs.com/package/@typemon/dynamodb-service) [![npm-downloads](https://img.shields.io/npm/dt/@typemon/dynamodb-service.svg)](https://www.npmjs.com/package/@typemon/dynamodb-service)
> DynamoDB with Dependency injection for TypeScript



## About
We wanted to use DynamoDB with OOP, dependency injection, and many other features.
But I did not find a satisfactory library and eventually I created it.
It is a DynamoDB service with very easy to use expressions and empty string handling.
I hope this helps.



## Features
- [With Expression builder](https://gitlab.com/monster-space-network/typemon/dynamodb-expression)
- [Empty string handling](#empty-string-handling)



## Installation
```
$ npm install @typemon/dynamodb-service
              @typemon/dynamodb-expression
              @typemon/di
              aws-sdk
```
```typescript
import {
    /* DynamoDB service class */
    DynamoDBService,

    /* Injection tokens */
    TABLE_NAME_TOKEN, OPTIONS_TOKEN,

    /* Types, Interfaces */
    Options, QueryOptions, QueryResult,

    /* Empty string namespace */
    EmptyString
} from '@typemon/dynamodb-service';
```



## Usage
### Although you can do direct instantiation, we recommend using injectors.
- Table name tokens are **`required`**.
```typescript
const injector: Injector = Injector.create([
    Provider.useValue({
        identifier: TABLE_NAME_TOKEN,
        value: 'monster-space-network'
    }),
    Provider.useValue({
        identifier: OPTIONS_TOKEN,
        value: {
            region: 'ap-northeast-2'
        }
    }),
    Provider.use(DynamoDBService)
]);

const dynamodb: DynamoDBService = injector.get(DynamoDBService);
```

### Put
```typescript
const typemon: Monster = {
    partition: 'monster', // partition key
    name: 'typemon',      // sort key
    description: 'TypeScript Monster',
    packages: [
        'ioc-container',
        'dynamodb-service',
        'dynamodb-expression'
    ]
};

await dynamodb.put(typemon);
```

### Get
```typescript
const typemon: Monster | null = await dynamodb.get({
    partition: 'monster', // partition key
    name: 'typemon',      // sort key
});

console.log(typemon);
```
```typescript
{
    partition: 'monster',
    packages: [
        'ioc-container',
        'dynamodb-service',
        'dynamodb-expression'
    ],
    description: 'TypeScript Monster',
    name: 'typemon'
}
```

### Update
```typescript
const updateExpressions: ReadonlyArray<UpdateExpression> = [
    listAppend('packages', [
        'serverless',
        'stack'
    ])
];
const typemon: Monster = await dynamodb.update({
    partition: 'monster', // partition key
    name: 'typemon',      // sort key
}, updateExpressions);

console.log(typemon);
```
```typescript
{
    packages: [
        'ioc-container',
        'dynamodb-service',
        'dynamodb-expression',
        'serverless',
        'stack'
    ],
    partition: 'monster',
    description: 'TypeScript Monster',
    name: 'typemon'
}
```

### Delete
```typescript
await dynamodb.delete({
    partition: 'monster', // partition key
    name: 'typemon',      // sort key
});
```

### Query
```typescript
const keyConditionExpression: Expression = equal('partition', 'monster');
const filterExpression: Expression = equal(size('packages'), 0);
const monsters: QueryResult<Monster> = await dynamodb.query(
    keyConditionExpression,
    filterExpression,
    { limit: 1 }
);

console.log(monsters);
```
```typescript
{
    items: [
        {
            partition: 'monster',
            packages: [],
            description: 'Minecraft Monster',
            name: 'minemon'
        }
    ],
    lastEvaluatedKey: { name: 'minemon', partition: 'monster' }
}
```



## Empty String Handling
- Handles `empty strings` and `identifiers` through `serialization` and `deserialization` functions.
```typescript
const item: Item = {
    emptyString: '',
    emptyStringInArray: [''],
    emptyStringInIndex: {
        emptyString: ''
    }
};
const serialized: Item = EmptyString.serialize(item);
const deserialized: Item = EmptyString.deserialize(serialized);

console.log(serialized, deserialized);
```
```typescript
{
    emptyString: 'EMPTY_STRING_IDENTIFIER',
    emptyStringInArray: ['EMPTY_STRING_IDENTIFIER'],
    emptyStringInIndex: {
        emptyString: 'EMPTY_STRING_IDENTIFIER'
    }
}
{
    emptyString: '',
    emptyStringInArray: [''],
    emptyStringInIndex: {
        emptyString: ''
    }
}
```

### If you want to `disable` the handling of empty strings, set the option to `false`.
```typescript
Provider.useValue(OPTIONS_TOKEN, {
    region: 'ap-northeast-2',
    useEmptyStringHandling: false // Disable empty string handling.
})
```
